package test;

public class FizzBuzz {
	public static void main(String[] args) {

		String printOut = valueJudgment(1);
		System.out.println(printOut);

	}
		public static String valueJudgment(int value) {

			String print;
			if ((value % 3 == 0) && (value % 5 == 0)) {
				print = "FizzBuzz";
				return print;
			} else if (value % 3 == 0) {
				print = "Fizz";
				return print;
			} else if (value % 5 == 0) {
				print = "Buzz";
				return print;
			} else {
				print = String.valueOf(value);
				return print;
			}
		}
}