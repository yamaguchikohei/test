/**
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author yamaguchi.kohei
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * {@link test.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void testMain() {
	}

	/**
	 * {@link test.FizzBuzz#valueJudgment(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testValueJudgment() {
		assertEquals("FizzBuzz", FizzBuzz.valueJudgment(15));
		assertEquals("Fizz", FizzBuzz.valueJudgment(3));
		assertEquals("Buzz", FizzBuzz.valueJudgment(5));
		assertEquals("1", FizzBuzz.valueJudgment(1));
	}

}
